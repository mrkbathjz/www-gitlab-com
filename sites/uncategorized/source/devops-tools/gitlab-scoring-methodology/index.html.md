---
layout: markdown_page
trial_bar: true
title: GitLab vs. Other DevOps Tools Infographic Scoring Methodology
description: "The purpose of the GitLab vs. Other DevOps Tools infographic is to provide our website visitors with an easily consumable visual representation of how GitLab compares to Other DevOps Tools."
canonical_path: "/devops-tools/gitlab-scoring-methodology/"
suppress_header: false
image_title: '/images/devops-tools/title_image.png'
---

## GitLab vs. "Other DevOps Tools" Infographic Scoring Methodology

The purpose of the GitLab vs. “Other DevOps Tools” infographic is to provide our website visitors with an easily consumable visual representation of how GitLab compares to “Other DevOps Tools”.  The infographics were prepared as part of an internal industry analysis by Product Managers (PMs) and Product Marketing Managers (PMMs) employed by GitLab and who GitLab views as subject experts in each stage of the DevOps Lifecycle.

PMs and PMMs were tasked with creating an objective grading scale to determine which features are required in each Stage of the DevOps Lifecycle based on their understanding of the industry and, using their best judgment, score both the “Other DevOps Tools” and GitLab on whether they offer such feature using publicly available data and resources.  A score of 1 was given in instances where a feature was completely available, 0.5 when a feature was partially available, and 0 when a feature was not available. 

Each Stage of the DevOps Lifecycle is researched by potentially a different PM and PMM team, which may lead to inconsistencies across the respective stages. This analysis reflects only the opinions of GitLab and not any of the Companies of the “Other DevOps tools”, including GitHub.  As such, there is no guarantee that this analysis reflects the most accurate and recent composition of required and offered features for each stage of the DevOps lifecycle.

Each Company's DevOps solution is categorized as either a Platform Solution or Point Tool Solution:  

- **Platform solutions** are scored based on all 10 stages of the DevOps Lifecycle.
- **Point tool solutions** are scored only in the stage of the DevOps Lifecycle that it aligns to

Therefore, the score GitLab or the “Other DevOps Tools” receives is:

- **Platform solutions:** Sum of total points across all 10 stages 
(maximum score of 128)

- **Point tool solution:** Sum of total points for the stage they align to 
(maximum score based on features identified for that stage)


### Score Mapping

Score of 0 - no support

Score of .5 - partial support

Score of 1 - full support


### Features by Stage

#### Manage (maximum score of 15)

- Data import
- Role-based Access control
- Audit logging
- Data export
- Group-level permissioning
- Audit reporting
- LDAP/AD integration
- Project-level permissioning
- Self-service onboarding
- 2FA
- Compliance framework enforcement
- DevOps Adoption reports
- Custom issue analytics
- Operational DevOps metrics
- Value stream management

#### Plan (maximum score of 16)

- Work Item Relationships
- Issue Management
- Global Search
- Requirements Management
- Workflow Management
- Extensibility
- Enterprise Framework Support
- Design Management
- Roadmaps
- Operational Metrics / Value Stream
- Capacity planning
- Boards
- End-to-end visibility / traceability
- Enterprise readiness
- Cost Tracking & Estimation
- Sprint Planning

#### Create (maximum score of 15)

- Git-Based SCM
- Protected branches
- Project and File Templates
- Code Review
- Web IDE
- Mirroring
- Change Approvals
- Live Preview
- Git LFS
- Signed Commits
- Snippets
- Partial Clone
- Push Rules
- Commit Graphs
- Scalable, Fault-Tolerant Git Storage

#### Verify (maximum score of 17)

- SaaS, On-prem, and hybrid installation
- Configuration-as-code
- Ease of use/setup
- Multi-cloud w/ cloud native support
- Embedded security scans in CI pipeline
- Dynamic pipelines
- Pipeline status logs and analytics for reporting
- Unit testing and static code analysis
- CI pipeline templates
- Pipeline scheduling and monitoring 
- Integration testing
- Enterprise readiness
- Linters, scanners, and syntax validation included 
- Load and performance testing
- Compliance and audit controls
- Build parallelization
- Pipeline visualization

#### Package (maximum score of 13)

- Package Registry
- High Availability
- Enterprise readiness
- Container registry
- API
- Searchable artifacts
- Virtual Registry
- Storage management
- Certified dependencies (or images)
- Dependency Firewall
- Dependency scanning
- Geo Replication
- Top package formats supported (npm, maven, pyi, etc.)

#### Secure (maximum score of 15)

- SAST
- Secrets detection
- coverage guided fuzz testing
- DAST
- Vulnerability mgmt
- API fuzz testing
- Dependency Scanning
- Vulnerability database
- Compliance dashboard
- Container scanning
- MR approvals
- compliance frameworks
- License Compliance
- Code Quality
- SBOM (dependency list)

#### Release (maximum score of 14)

- Blue-green deployment
- Support for cloud native deployment targets
- Deployment pipeline visualization
- Canary deployment
- DORA metrics (Mean time for changes, MTTR, Change Failure Rate, Deployment Frequency)
- Environments dashboard
- incremental roll-out
- Roll back
- Static Site Hosting
- Feature flags
- Runbooks
- MR approvals
- Support for traditional deployment targets - physical, virtual machines, mainframes
- Release versioning and evidence

#### Configure (maximum score of 7)

- Infrastructure/ Config/ Policies etc as code
- Secure Kubernetes Integration
- Integration with IaC tools like Terraform
- Push and Pull actions
- Configure physical, virtual, cloud infrastructures
- Configure cloud native environments
- Detect and remediate configuration drift

#### Monitor (maximum score of 12)

- Infrastructure Performance monitoring
- Automated discovery & mapping
- Log monitoring
- Application performance monitoring
- Mobile Performance Monitoring
- Alert Management
- Digital experience monitoring (real user monitoring)
- Synthetic monitoring
- Distributed Tracing
- Incident Management
- Load performance testing
- Integrations with established incident management tools

#### Protect (maximum score of 4)

- container host security
- container scanning
- container network security
- security orchestration (runtime policies)
