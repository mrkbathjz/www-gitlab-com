---
layout: job_family_page
title: "Go-to-Market Planning and Operations"
description: "The Go-to-Market (GTM) Planning and Operations job family supports GitLab's go-to-market planning and compensation design, as well as the associated analytics and tools."
---

The Go-to-Market (GTM) Planning and Operations job family supports GitLab's go-to-market planning and compensation design, as well as the associated analytics and tools. They partner cross functionally across the Chief Revenue Officer's organization.

## Responsibilities

* Iterate on the Field Planning & Bottoms-up Quota Capacity Models; maintain throughout the year and use to drive insights that improve efficiency of the GTM motion
* Drive all aspects of compensation design for roles across the CRO organization (including core Sales, Customer Success, Channel, Alliances, Management and others); Collaborate closely with Sales, Finance, Sales Commissions & Ops in the process
* Deliver insightful comp analytics that lead to improvements in the design and effectiveness of compensation models 
* Support quota/compensation troubleshooting throughout the year 
* Support the evaluation and implementation of software tools required for Field, Quota and Territory planning

## Requirements

* Willingness to partner, collaborate and influence across functional areas (e.g. Finance, People Operations and Sales) and support multiple business partners
* Excellent problem solving, project management, interpersonal and organizational skills
* Previous experience in Consulting, Banking, PE or Strategy roles a big plus 
* Basic SQL skills a big plus; we have experts, but it helps tremendously 
* SFDC and Xactly familiarity and knowledge of typical enterprise SaaS tools 
* SaaS and B2B experience preferred
* Interest in GitLab, and open source software
* Share our [values](/handbook/values/), and work in accordance with those values.
* Ability to use GitLab

## Levels

### Analyst, GTM Planning and Operations

The Analyst, GTM Planning and Operations reports to the [Director, GTM Planning & Operations](/job-families/sales/gtm-planning-and-operations/).

#### Analyst, GTM Planning and Operations Job Grade
The Analyst, GTM Planning and Operations is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Analyst, GTM Planning and Operations Responsibilities

* Provide the CRO organization with thoughtful insights about the effectivness of Go-To-Market planning initiatives
* Participate in the execution of the annual Go-To-Market planning process for the CRO organization
* Partner closely with Sales Finance and the rest of Field Operations to help design an efficient, effective and predictable GTM motion

#### Analyst, GTM Planning and Operations Requirements

* BA/BS degree in engineering, accounting, finance, economics or other quantitative fields preferred
* Advanced analytical and financial modeling skills with high motivation to drive high value insights 
* Some experience with go-to-market design & sales planning initiatives
* Experience collaborating closely with leaders at the Sr. Manager level and below

### Senior Analyst, GTM Planning and Operations

The Senior Analyst, GTM Planning and Operations reports to the [Director, GTM Planning & Operations](/job-families/sales/gtm-planning-and-operations/).

#### Senior Analyst, GTM Planning and Operations Job Grade

The Senior Analyst, GTM Planning and Operations is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Analyst, GTM Planning and Operations Responsibilities

* Be a key driver and architect of the annual Go-To-Market planning process for the CRO organization
* Provide the CRO organization with thoughtful insights about the effectivness of Go-To-Market planning initiatives
* Partner closely with Sales Finance and the rest of Field Operations to help design an efficient, effective and predictable GTM motion
* Participate in the design and implementation of Go-To-Market experimentation (e.g. Land vs. Expand pilots)

#### Senior Analyst, GTM Planning and Operations Requirements

* BA/BS degree in engineering, accounting, finance, economics or other quantitative fields preferred
* Advanced analytical and financial modeling skills with high motivation to drive high value insights 
* Relevant experience and a solid understanding of go-to-market design & sales planning
* Experience collaborating closely with leaders at the Director level and below

### GTM Planning and Operations Manager

The GTM Planning and Operations Manager reports to the [Director, GTM Planning & Operations](/job-families/sales/gtm-planning-and-operations/).

#### GTM Planning and Operations Manager Job Grade

The GTM Planning and Operations Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

**Market Justification:** 
From a survey data perspective, this role rolls up to the broader Sales Operations job match where 91 companies have a grade 8 level with an average of 3 employee incumbents within the tech market. The business justification for a Go-To-Market Planning & Operations Manager as an individual contributor is to attract experienced top-tier talent. The Go-To-Market Planning & Operations Manager role requires a highly skilled individual capable of leading the decision-making process regarding complex planning and strategy matters for a 500+ Field Sales team. As GitLab and its Sales team continue to grow exponentially, the role will continue to require navigating an ever-more complex network of 170+ prime Sellers, 100+ Sales leaders, 15+ unique roles, 7+ Vice Presidents, along with considerable time interfacing with the CRO and CFO on all matters Go-To-Market planning. In the marketplace, similar IC roles can be found at Google, Salesforce, an Twilio.

#### GTM Planning and Operations Manager Responsibilities

* Be a key driver and architect of the annual Go-To-Market planning process for the CRO organization
* Play a leadership role in providing the CRO organization with thoughtful insights about the effectivness of Go-To-Market planning initiatives
* Partner closely with Sales Leadership, Finance and Sales Operations to help design an efficient, effective and predictable GTM motion
* Be a leader in the design and implementation of Go-To-Market experimentation (e.g. Land vs. Expand pilots)

#### GTM Planning and Operations Manager Requirements

* BA/BS degree in engineering, accounting, finance, economics or other quantitative fields preferred
* Relevant experience and a solid understanding of go-to-market design & sales planning as well as sales compensation design & analytics 
* Some experience interacting with Leaders at the VP level as well as experience collaborating closely with leaders at the Sr. Director level and below
* Advanced analytical and financial modeling skills with high motivation to drive high value insights 
* Meet the [Leadership at Gitlab](https://about.gitlab.com/company/team/structure/#management-group) requirements

### Senior Manager, GTM Planning and Operations

The Senior Manager, GTM Planning and Operations reports to the [Director, GTM Planning & Operations](/job-families/sales/gtm-planning-and-operations/).

#### Senior Manager, GTM Planning and Operations Job Grade

The Senior Manager, GTM Planning and Operations is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager, GTM Planning and Operations Responsibilities

* Be a key driver and architect of the annual Go-To-Market planning process for the CRO organization
* Play a leadership role in providing the CRO organization with thoughtful insights about the effectivness of Go-To-Market planning initiatives
* Partner closely with Sales Leadership, Finance and Sales Operations to help design an efficient, effective and predictable GTM motion
* Be a key leader in the design and implementation of Go-To-Market experimentation (e.g. Land vs. Expand pilots)
* Build, develop, and lead a high functioning team of Go-To-Market planning specialists

#### Senior Manager, GTM Planning and Operations Requirements

* BA/BS degree in engineering, accounting, finance, economics or other quantitative fields preferred
* Considerable relevant experience and a solid understanding of go-to-market design & sales planning as well as sales compensation design & analytics 
* Considerable experience interacting with Leaders at the VP level as well as experience collaborating closely with leaders at the Sr. Director level and below
* Advanced analytical and financial modeling skills with high motivation to drive high value insights 
* Meet the [Leadership at Gitlab](https://about.gitlab.com/company/team/structure/#management-group) requirements

### Director, GTM Planning and Operations

The Director, GTM Planning and Operations reports to the [Sr. Director, Sales Strategy](/job-families/sales/sales-strategy/).

#### Director, GTM Planning and Operations Job Grade

The Director, GTM Planning and Operations is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, GTM Planning and Operations Responsibilities

* Design and drive the annual Go-To-Market planning process for the CRO organization
* Own the Field Planning & Bottoms-up Quota Capacity Models; maintain throughout the year and use to drive insights that improve efficiency of the GTM motion
* Responsible for the design and implementation of Go-To-Market experimentation (e.g. Land vs. Expand pilots)
* Deliver structured, actionable and insightful comp insights that lead to improvements in the design and effectiveness of compensation models 
* Be a thought leader in quota/compensation troubleshooting throughout the year 
* Drive the evaluation and implementation of software tools required for Field, Quota and Territory planning
* Build, develop, and lead a high functioning team of Go-To-Market planning specialists

#### Director, GTM Planning and Operations Requirements

* BA/BS degree in engineering, accounting, finance, economics or other quantitative fields preferred
* Significant experience in relevant fields and a comprehensive understanding of go-to-market design & sales planning as well as sales compensation design & analytics
* Significant experience interacting with C-suite Executives and closely partnering with Leaders at the VP level and below
* Advanced analytical and financial modeling skills with a superb ability to drive high value insights
* Meet the [Leadership at Gitlab](https://about.gitlab.com/company/team/structure/#management-group) requirements

## Performance Indicators

* [IACV vs. plan > 1](/handbook/sales/#incremental-annual-contract-value-iacv)
* [IACV efficiency > 1.0](/handbook/sales/#iavc-efficiency-ratio)
* [Win rate > 30%](/handbook/sales/#win-rate)
* [Rep IACV per comp > 5](/handbook/sales/#measuring-sales-rep-productivity)

## Career Ladder 

The next steps in the GTM Planning and Operations job family is not yet defined at GitLab.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](https://about.gitlab.com/company/team/).

- Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters,
- Next, candidates will be invited to schedule a 25 minute interview with the Hiring Manager,
- Next, candidates can expect 2-5 separate 25 minute interviews with other Team Members, 
- Finally, candidates will be invited to schedule a 50 minute interview with an Executive. 

Additional details about our process can be found on our [hiring page](/handbook/hiring).
