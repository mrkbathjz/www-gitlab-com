---
layout: handbook-page-toc
title: "Security Risk Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<!--HTML Parser Markup-->
{::options parse_block_html="true" /}

# Security Risk Team

## <i class="fas fa-bullseye" style="color:rgb(110,73,203)" aria-hidden="true"></i> Mission
{: #bullseye-light-purple}

This page is under construction.

## <i class="far fa-lightbulb" style="color:rgb(110,73,203)" aria-hidden="true"></i> Core Compentencies
{: #lightbulb-light-purple}
----

### <i class="fas fa-shield-alt" style="color:rgb(253,109,38)" aria-hidden="true"></i> Security Operational Risk Management (StORM) Program
{: #storm-md-orange}

A Tier 2 Operational Risk Management program which focuses on the identification, assessment, tracking, and overall management of operational security risks across the organization. Check out the [StORM Program & Procedures](/handbook/engineering/security/security-assurance/security-risk/storm-program/index.html) handbook page for additional details, including a quick introduction to Risk Management at GitLab as well as information about the purpose, scope, and specific procedures executed as part of the program. 


<div class="panel panel-gitlab-orange">
**Need to communicate a potential risk to the team?**
{: .panel-heading}
<div class="panel-body">

Please refer to the [communication section of the StORM Program & Procedures](/handbook/engineering/security/security-assurance/security-risk/storm-program/index.html#communication-of-risks-to-the-security-risk-team) page for information on the various ways that team members can use to escalate potential risks to the Security Risk Team.

</div>
</div>

----

### <i class="fas fa-search" style="color:rgb(253,109,38)" aria-hidden="true"></i> Critical System Tiering
{: #cst-md-orange}

Every system at GitLab is assigned a critical system tier. The Security Risk Team owns the tiering methodology that establishes each system's tier. For more information about the methodology and inputs utilized to determine tiering, refer to the [Critical Systems Tiering Methodology](/handbook/engineering/security/security-assurance/security-risk/storm-program/critical-systems.html) handbook page.

----

### <i class="fas fa-exclamation-triangle" style="color:rgb(253,109,38)" aria-hidden="true"></i> Business Impact Analysis (BIA)
{: #bia-md-orange}

On an annual cadence, the Security Risk Team conducts a BIA over systems utilized across GitLab. The data collected as part of this process is used to ensure that various data sources, such as system inventories, are continuously maintained and up-to-date. For more information about the BIA process and procedures, refer to the [Business Impact Analysis](/handbook/engineering/security/security-assurance/security-risk/storm-program/business-impact-analysis.html) handbook page.

----

### <i class="fas fa-hands-helping" style="color:rgb(253,109,38)" aria-hidden="true"></i> Third Party Risk Management (TPRM) Program
{: #tprm-md-orange}

:construction: :construction: :construction:

This section is under construction

:construction: :construction: :construction:

----

## <i class="fas fa-users" style="color:rgb(110,73,203)" aria-hidden="true"></i> Security Risk Team Roles & Responsibilities
{: #users-light-purple}

|Team Member|Role|Responsibilities|
|:----------:|:----------:|:---------------:|
|TBD|Manager, Security Risk||
|[Steve Truong](https://gitlab.com/sttruong)|[Senior Security Risk Engineer](https://about.gitlab.com/job-families/engineering/security-risk/#senior-security-risk-engineer)|StORM Program [DRI](/handbook/people-group/directly-responsible-individuals/)|
|[Darren Lamison-White](https://gitlab.com/DLWhite0322)|[Senior Security Risk Engineer](https://about.gitlab.com/job-families/engineering/security-risk/#senior-security-risk-engineer)|TPRM Program [DRI](/handbook/people-group/directly-responsible-individuals/)|

## <i class="fas fa-id-card" style="color:rgb(110,73,203)" aria-hidden="true"></i> Contact the Security Risk Team
{: #contact-card-light-purple}

- <i class="fas fa-envelope fa-fw" style="color:rgb(219,59,33)" aria-hidden="true"></i> Email: 
   - `TBD`
- <i class="fab fa-slack fa-fw" style="color:rgb(219,59,33)" aria-hidden="true"></i> Slack: 
   - [#security-risk-management channel](https://gitlab.slack.com/archives/C01EKDNRVFD)
   - [#sec-assurance channel](https://gitlab.slack.com/archives/C0129P7DW75) (includes the broader Security Assurance Team)
   - `@TAG TBD`
- <i class="fab fa-gitlab fa-fw" style="color:rgb(219,59,33)" aria-hidden="true"></i> GitLab:
   - Tag the team across GitLab using `@gitlab-com/gl-security/security-assurance/security-risk-team`

## <i class="fas fa-book" style="color:rgb(110,73,203)" aria-hidden="true"></i> References
{: #book-light-purple}



